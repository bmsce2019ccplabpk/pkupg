#include<stdio.h>
int main()
{
   int t,*p,q,x,y;
   printf("Enter x and y values \n");
   scanf("%d%d",&x,&y);
   printf("Before swap x=%d y=%d\n ",x,y);
   swap(&x,&y);
   printf("After swap x=%d,y=%d",x,y);
   return 0;
}
  
void swap(int *p,int *q)
{
    int t;
    t=*p;
    *p=*q;
    *q=t;
}